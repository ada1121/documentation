//
//  Cal_listModel.swift
//  FoodTracking
//
//  Created by Ubuntu on 1/6/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class Category_listModel : CustomStringConvertible, Comparable {
    var description: String{ return "ImageFile with ID: \(price)" }
    
    
    static func < (lhs: Category_listModel, rhs: Category_listModel) -> Bool {
        return lhs.price == rhs.price
    }
    
    static func == (lhs: Category_listModel, rhs: Category_listModel) -> Bool {
        return lhs.price < rhs.price
    }
    
    var name : String = ""
    var price : Float = 0.0
    var rank : String? = nil
    var other  : String = ""
    var id : Int = 0
    init(name : String,price : Float,rank : String,other: String) {
        self.name = name
        self.price = price
        self.rank = rank
        self.other = other
    }
    
}
