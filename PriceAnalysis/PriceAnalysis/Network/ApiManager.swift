//
//  ApiManager.swift
//  PortTrucker
//
//  Created by PSJ on 9/12/19.
//  Copyright © 2019 LiMing. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON

// ************************************************************************//
                                // Food Tracking //
// ************************************************************************//

let API = "http://34.225.255.196/api/"

let LOGIN = API + "signin"
let SIGNUP = API + "signup"
let FORGOT = API + "forgot"
let GETFOODLIST = API + "getFoodList"

struct PARAMS {
    
    // ************************************************************************//
                                    // FoodTracking //
    // ************************************************************************//
    static let STATUS = "result_code"
    static let TRUE = "0"
    static let USERINFO = "user_info"
    static let EMAIL = "email"
    static let USERNAME = "username"
    static let PASSWORD = "password"
    static let FOODLIST = "food_list"
    static let ID = "food_list"
    static let TITLE = "title"
    static let RECOMMENDED = "recomended"
    static let CALORIELIST = "calorie_list"
    static let CAL = "cal"
    static let CABS = "carbs"
    static let FAT = "fat"
    static let PROTEIN = "protein"
    static let FIBER = "fiber"
}

class ApiManager {
    
    class func login(useremail : String, password : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["email" : useremail, "password" : password ] as [String : Any]
        Alamofire.request(LOGIN, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue // 0,1,2
                    let user_info = dict[PARAMS.USERINFO].object
                    let userdata = JSON(user_info)
                    if status == PARAMS.TRUE {
                        completion(true, userdata)
                    } else {
                        completion(false, status)
                    }
            }
        }
    }
    
    class func signup( username : String, email : String, password : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
          let requestURL = SIGNUP
                 Alamofire.upload(
                     multipartFormData: { multipartFormData in
                        multipartFormData.append( username.data(using:String.Encoding.utf8)!, withName: PARAMS.USERNAME)
                        multipartFormData.append( email.data(using:String.Encoding.utf8)!, withName: PARAMS.EMAIL)
                        multipartFormData.append( password.data(using:String.Encoding.utf8)!, withName: PARAMS.PASSWORD)

                 },
                     to: requestURL,
                     encodingCompletion: { encodingResult in
                         switch encodingResult {
                             case .success(let upload, _, _):
                                 upload.responseJSON { response in
                                     switch response.result {
                                         case .failure: completion(false, nil)
                                         case .success(let data):
                                             let dict = JSON(data)
                                             print(dict)
                                             let status = dict[PARAMS.STATUS].stringValue
                                             if status == PARAMS.TRUE {
                                                 completion(true, dict)
                                             } else {
                                                 completion(false, status)
                                             }
                                         }
                                 }
                             case .failure(let encodingError):
                                 print(encodingError)
                                 completion(false, nil)
                             }
                 })
    }

    class func forgot(email : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
            let params = [PARAMS.EMAIL: email] as [String : Any]
            Alamofire.request( FORGOT, method:.post, parameters:params)
                .responseJSON { response in
                    switch response.result {
                        case .failure:
                            completion(false, nil)
                        case .success(let data):
                            let dict = JSON(data)
                            let status = dict[PARAMS.STATUS].stringValue
                            let pincode = dict["pincode"].stringValue
                            if status == PARAMS.TRUE {
                                completion(true, pincode)
                            } else {
                                completion(false, status)
                            }
                        }
            }
        }
    
    class func getFoodList(completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        Alamofire.request(GETFOODLIST, method:.post)
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue // 0,1,2
                    let food_cat = dict[PARAMS.FOODLIST].arrayObject
                    let fooddata = JSON(food_cat as Any)
                    if status == PARAMS.TRUE {
                        //print("this is fooddata =====",fooddata as Any)
                        completion(true, fooddata)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
}
