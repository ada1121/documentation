//
//  BaseVC1.swift
//  Loyaltiyapp
//
//  Created by PSJ on 10/21/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import SwiftyJSON
import KRProgressHUD
import KRActivityIndicatorView

class BaseVC1 : UIViewController {
    
    var gradientLayer: CAGradientLayer!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func createGradientView(_ targetView : UIView) {
        
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = targetView.bounds
        gradientLayer.colors = [UIColor.red.cgColor, UIColor.yellow.cgColor]
        targetView.layer.addSublayer(gradientLayer)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
    }
    
    func createGradientLabel(_ targetView : UIView, letter : String,fontsize : Int ,position : Int) {
        
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = targetView.bounds
        gradientLayer.colors = [UIColor.red.cgColor, UIColor.yellow.cgColor]
        targetView.layer.addSublayer(gradientLayer)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        // Create a label and add it as a subview
        let label = UILabel(frame: targetView.bounds)
        label.text = letter
        label.font = UIFont.boldSystemFont(ofSize: CGFloat(fontsize))
        
        if position == 0 {
            label.textAlignment = .center
        }
        else if position == 1{
            label.textAlignment = .left
        }
        
        else if position == 2{
            label.textAlignment = .right
        }
        
        targetView.addSubview(label)
        
        // Tha magic! Set the label as the views mask
        targetView.mask = label
    }
    
    func transitionAnimation(view: UIView, animationOptions: UIView.AnimationOptions, isReset: Bool) {
      UIView.transition(with: view, duration: durationOfAnimationInSecond, options: animationOptions, animations: {
        view.backgroundColor = UIColor.init(named: isReset ? "darkGreen" :  "darkRed")
      }, completion: nil)
    }
    
//    func showMessage1(_ message : String) {
//        self.view.makeToast(message)
//    }
    
    func setProgressHUDStyle(_ style: Int,backcolor: UIColor,textcolor : UIColor, imagecolor : UIColor ) {
        
            if style != 2{
            let styles: [KRProgressHUDStyle] = [.white, .black, .custom(background: #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1), text: #colorLiteral(red: 0.1294117719, green: 0.2156862766, blue: 0.06666667014, alpha: 1), icon: #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1))]
                KRProgressHUD.set(style: styles[style]) }
            
            else{
                let styles : KRProgressHUDStyle = .custom (background:backcolor,text : textcolor, icon: imagecolor )
                 KRProgressHUD.set(style: styles)
                
            }
    }

    class func isString10Digits(_ ten_digits: String) -> Bool{

        if !ten_digits.isEmpty {

            let numberCharacters = NSCharacterSet.decimalDigits.inverted
            return !ten_digits.isEmpty && ten_digits.rangeOfCharacter(from: numberCharacters) == nil
        }
        return false
    }
    
   func showProgressSet_withMessage(_ msg : String, msgOn : Bool,styleVal: Int ,backColor: UIColor,textColor : UIColor,imgColor : UIColor , headerColor : UIColor, trailColor : UIColor)  {
    
    setProgressHUDStyle(styleVal,backcolor: backColor,textcolor : textColor, imagecolor: imgColor)
        KRProgressHUD.set(activityIndicatorViewColors: [headerColor, trailColor])
        KRProgressHUD.show(withMessage: msgOn == false ? nil : msg)
    }
    
    func progressSet(styleVal: Int ,backColor: UIColor,textColor : UIColor , imgcolor: UIColor, headerColor : UIColor, trailColor : UIColor)  {
    
        setProgressHUDStyle(styleVal,backcolor: backColor,textcolor : textColor, imagecolor: imgcolor)
        KRProgressHUD.set(activityIndicatorViewColors: [headerColor, trailColor])
        
    }

    func progShowSuccess(_ msgOn:Bool, msg:String){
        KRProgressHUD.showSuccess(withMessage: msgOn == false ? nil : msg)
    }

    func progShowError(_ msgOn:Bool, msg:String) {
        KRProgressHUD.showError(withMessage: msgOn == false ? nil : msg)
    }

    func progShowWithImage(_ msgOn:Bool, msg:String ,image : String) {
        let image = UIImage(named:image)!
        KRProgressHUD.showImage(image, message: msgOn == false ? nil : msg)
    }
    
    func progShowInfo(_ msgOn:Bool, msg:String) {
        KRProgressHUD.showInfo(withMessage: msgOn == false ? nil : msg)
    }
    
    func showProgress() {
        KRProgressHUD.show()
    }
    func hideProgress() {
        KRProgressHUD.dismiss()
    }
    
    func resetProgress() {
        KRProgressHUD.resetStyles()
    }
    
    func alertMake(_ msg : String) -> UIAlertController {
        alertController = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        alertController!.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alertController!
    }
    
    func alertDisplay(alertController: UIAlertController) {
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func gotoVC(_ nameVC: String){
        
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: nameVC)
        toVC!.modalPresentationStyle = .fullScreen
        self.present(toVC!, animated: false, completion: nil)
    }
    
    func gotoNavPresent1(_ storyname : String) {
              
          let toVC = self.storyboard?.instantiateViewController(withIdentifier: storyname)
          toVC?.modalPresentationStyle = .fullScreen
          self.navigationController?.pushViewController(toVC!, animated: false)
            
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: false)
    }
    func goBackHome() {
        self.navigationController?.popToRootViewController(animated: false)
    }
    func goSetting() {
       let toVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC")
       toVC?.modalPresentationStyle = .fullScreen
       self.navigationController?.pushViewController(toVC!, animated: false)
    }
    
    func navBarHidden() {
       self.navigationController?.isNavigationBarHidden = true
   }
    func navBarTransparent1() {
      self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
      self.navigationController?.navigationBar.shadowImage = UIImage()
      self.navigationController?.navigationBar.isTranslucent = true
      self.navigationController?.view.backgroundColor = .clear
    }
    
    func navBarwithColor() {
         self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
         self.navigationController?.navigationBar.shadowImage = UIImage()
         self.navigationController?.navigationBar.isTranslucent = true
         self.navigationController?.view.backgroundColor = .blue
   }
    
// slider navigation storyboard creation
    
    func setEdtPlaceholderColor(_ edittextfield : UITextField , placeholderText : String, placeColor : UIColor)  {
        edittextfield.attributedPlaceholder = NSAttributedString(string: placeholderText,
        attributes: [NSAttributedString.Key.foregroundColor: placeColor])
    }
}


extension UITextField {
    
    enum PaddingSide1 {
        case left(CGFloat)
        case right(CGFloat)
        case both(CGFloat)
    }
    
    func addPadding1(_ padding: PaddingSide1) {
        
        self.leftViewMode = .always
        self.layer.masksToBounds = true
        
        switch padding {
            
        case .left(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.rightViewMode = .always
            
        case .right(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.rightView = paddingView
            self.rightViewMode = .always
            
        case .both(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.leftViewMode = .always
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
}

extension UIView {
    func dropShadowleft1(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.white.cgColor
        layer.shadowOpacity = 0.8
        layer.shadowOffset = CGSize(width: -1.0, height: 1.0)
        layer.shadowRadius = 3.0
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func ShadowCenter1(scale: Bool = true) {
           layer.masksToBounds = false
           layer.shadowColor = UIColor.white.cgColor
           layer.shadowOpacity = 0.8
           layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
           layer.shadowRadius = 3.0
           layer.shouldRasterize = true
           layer.rasterizationScale = scale ? UIScreen.main.scale : 1
       }
    
    func dropShadowbottom1(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.white.cgColor
        layer.shadowOpacity = 0.7
        layer.shadowOffset = CGSize(width: 0.0, height:4.0)
        layer.shadowRadius = 2.0
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
}

extension UIViewController {

    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)

        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }

        var color: UInt32 = 0
        scanner.scanHexInt32(&color)

        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask

        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0

        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }

    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0

        getRed(&r, green: &g, blue: &b, alpha: &a)

        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0

        return String(format:"#%06x", rgb)
    }
}

extension UIButton {
    func configure(color: UIColor = .blue, font: UIFont = UIFont.boldSystemFont(ofSize: 12)) {
        self.setTitleColor(color, for: .normal)
        self.titleLabel?.font = font
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 5.0
    }

    func configure(icon: UIImage, color: UIColor? = nil) {
        self.setImage(icon, for: .normal)
        if let color = color {
            tintColor = color
        }
    }

    func configure(color: UIColor = .blue,
                   font: UIFont = UIFont.boldSystemFont(ofSize: 12),
                   cornerRadius: CGFloat,
                   borderColor: UIColor? = nil,
                   backgroundColor: UIColor,
                   borderWidth: CGFloat? = nil) {
        self.setTitleColor(color, for: .normal)
        self.titleLabel?.font = font
        self.backgroundColor = backgroundColor
        if let borderColor = borderColor {
            self.layer.borderColor = borderColor.cgColor
        }
        if let borderWidth = borderWidth {
            self.layer.borderWidth = borderWidth
        }
        self.layer.cornerRadius = cornerRadius
    }
}

extension UITextField {
    func configure(color: UIColor = .blue,
                   font: UIFont = UIFont.boldSystemFont(ofSize: 12),
                   cornerRadius: CGFloat,
                   borderColor: UIColor? = nil,
                   backgroundColor: UIColor,
                   borderWidth: CGFloat? = nil) {
        if let borderWidth = borderWidth {
            self.layer.borderWidth = borderWidth
        }
        if let borderColor = borderColor {
            self.layer.borderColor = borderColor.cgColor
        }
        self.layer.cornerRadius = cornerRadius
        self.font = font
        self.textColor = color
        self.backgroundColor = backgroundColor
    }
}



extension DefaultsKeys {
    
    static let id = DefaultsKey<String?>("id")
    static let username = DefaultsKey<String?>("username")
    static let email = DefaultsKey<String?>("email")
    static let profile = DefaultsKey<String?>("profile")
    static let pwd = DefaultsKey<String?>("pwd")
    //static let calorieSetState = DefaultsKey<Int?>("calorieSetState")
    static let oldValue = DefaultsKey<String?>("oldValue")
    
}

extension UITextField {
    @IBInspectable var placeholderColor: UIColor {
        get {
            return attributedPlaceholder?.attribute(.foregroundColor, at: 0, effectiveRange: nil) as? UIColor ?? .clear
        }
        set {
            guard let attributedPlaceholder = attributedPlaceholder else { return }
            let attributes: [NSAttributedString.Key: UIColor] = [.foregroundColor: newValue]
            self.attributedPlaceholder = NSAttributedString(string: attributedPlaceholder.string, attributes: attributes)
        }
    }

}



