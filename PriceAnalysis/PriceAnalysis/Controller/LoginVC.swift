//
//  LoginVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/30/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox
import KRProgressHUD
import KRActivityIndicatorView

class LoginVC: BaseVC1 , ValidationDelegate, UITextFieldDelegate {

    @IBOutlet weak var loginCaption: UILabel!
    @IBOutlet weak var loginView: UIView!
    
    @IBOutlet weak var edtEmail: UITextField!
    @IBOutlet weak var edtPwd: UITextField!
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editInit()
        loginCaption.text = "LOGIN HERE"
        loadLayout()
        self.hideKeyboardWhenTappedAround()
    }
    
    func editInit()  {
        setEdtPlaceholderColor(edtEmail, placeholderText: "Email", placeColor: UIColor.gray)
        setEdtPlaceholderColor(edtPwd, placeholderText: "Password", placeColor: UIColor.gray)
    }
    func loadLayout() {
        edtEmail.text = "teddy@gmail.com"
        edtPwd.text = "123"
    }
    
    override func viewWillAppear(_ animated: Bool) {
            createGradientView(loginView)
    }
    @IBAction func forgotBtnClicked(_ sender: Any) {
        gotoVC("ForgotVC")
    }
    
    @IBAction func signBtnClicked(_ sender: Any) {
        gotoVC("SignUpVC")
    }
    
    @IBAction func loginBtnClicked(_ sender: Any) {
        validator.registerField(edtEmail, errorLabel: nil , rules: [ RequiredRule(),EmailRule()])
        validator.registerField(edtPwd, errorLabel: nil , rules: [RequiredRule(),AlphaNumericRule()])
        validator.styleTransformers(success:{ (validationRule) -> Void in
            
        }, error:{ (validationError) -> Void in
            print("error")
            self.progressSet( styleVal: 2, backColor: UIColor.init(named: "ColorBlur")!, textColor: .white, imgcolor: .red, headerColor: .red, trailColor: .yellow)
            self.progShowError(true, msg: "Incorrect Email or Password")
        })
        validator.validate(self)
    }
    
    func validationSuccessful() {
        loginApi(useremail: edtEmail.text!, pwd: edtPwd.text!)
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
    }
    
    func loginApi(useremail : String, pwd : String){
        
        showProgressSet_withMessage("Connecting... \n Just a moment!", msgOn: true, styleVal: 2, backColor: UIColor.init(named: "ColorBlur")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        
        ApiManager.login(useremail: useremail, password: pwd) { (isSuccess, data) in
            self.hideProgress()
            
            if isSuccess{
                  self.loginSuccess()
            }
            else{
                if data == nil{
                }
                else{
                    let result_message = data as! String
                    if(result_message == "1"){
                    }
                    else{
                    }
                }
            }
        }
    }
    
    func loginSuccess() {
        gotoVC("BreakFastVC")
    }
}
