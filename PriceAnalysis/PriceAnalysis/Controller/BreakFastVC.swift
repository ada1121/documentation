//
//  BreakFastVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/28/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import ExpyTableView
import SwiftyMenu
import ExpandableCell
import SwiftyUserDefaults

class BreakFastVC: BaseVC1 {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var heightContraint: NSLayoutConstraint!
    
    @IBOutlet weak var addFoodListHeight: NSLayoutConstraint!
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var saveBtnView: UIView!
    @IBOutlet weak var saveLabel: UILabel!
    var cellHeight = 60
    let count_num = 0
    @IBOutlet weak var edtCatName: UITextField!
    @IBOutlet weak var edtPrice: UITextField!
    @IBOutlet weak var edtOtherPrice: UITextField!
    
    @IBOutlet weak var valueTableGroup: UIView!
    var catListdataSource = [Category_listModel]()
    var clickNum = 0
    var analysis = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        createGradientLabel(titleView, letter: "Create & Analysis Price", fontsize: 24, position: 1)
        createGradientView(saveBtnView)
        saveLabel.text = "ADD"
        saveLabel.textColor = .white
        saveBtnView.addSubview(saveLabel)
        // collectionView autoHeight control
        collectionView.delegate = self
        collectionView.dataSource = self
        let height =  collectionView.collectionViewLayout.collectionViewContentSize.height
        heightContraint.constant = height
        self.view.layoutIfNeeded()
        // ********  add FoodList Height determine *******//
        addFoodListHeight.constant = UIScreen.main.bounds.width / 9
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        let width = UIScreen.main.bounds.size.width
        layout.itemSize = CGSize(width: width, height: width/10.0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView!.collectionViewLayout = layout
        
        initCollectionTextField()
     }
    
    func initCollectionTextField() {
        edtCatName.attributedPlaceholder = NSAttributedString(string: "Input category name",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightText])
        edtPrice.attributedPlaceholder = NSAttributedString(string: "Input price",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightText])
        edtOtherPrice.attributedPlaceholder = NSAttributedString(string: "Input other",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightText])
    }
    @IBAction func sampleBtnClicked(_ sender: Any) {
        self.gotoVC("LoginVC")
    }
    @IBAction func gotoHome(_ sender: Any) { // analysis action
        
        if analysis == true {
        let sortedArray = catListdataSource.sorted() {
            $0.price < $1.price
        }
        print(sortedArray)
        
        for i in 0...catListdataSource.count - 1 {
            for j in 0...sortedArray.count - 1 {
                if catListdataSource[i].id == sortedArray[j].id {
                    catListdataSource[i].rank = "\(j + 1)"
                    continue
                }
            }
        }
        print("catListdataSource: \(catListdataSource)")
            collectionView.reloadData()
            //clickNum = clickNum + 1
            analysis = false
        }
        else{
            return}
        
    }
    @IBAction func saveBtnClicked(_ sender: Any) { // save action
        let name = edtCatName.text!
        var price = 0.00
        price = (edtPrice.text?.toDouble())!
        //let isnumber = BaseVC1.isString10Digits(edtPrice.text!)
        let other = edtOtherPrice.text!
        let id = u_id
        if name == "" || price == 0.00 || price < 0.01 {
            edtPrice.text = ""
            self.progressSet( styleVal: 2, backColor: UIColor.init(named: "ColorDarkBlur")!, textColor: .white, imgcolor: .blue, headerColor: .red, trailColor: .yellow)
            self.progShowInfo(true, msg: "Please Input Correctly ! \n Price must be number value !")
            return
        }
        else{
            let one = Category_listModel( name: name, price: Float(price), rank: "", other: other)
            one.id = id
            catListdataSource.append(one)
            collectionView.reloadData()
            edtCatName.text = ""
            edtPrice.text = ""
            edtOtherPrice.text = ""
            u_id = u_id + 1
            let height =  collectionView.collectionViewLayout.collectionViewContentSize.height
            heightContraint.constant = height
            self.view.layoutIfNeeded()
            //clickNum = 1
            analysis = true
        }
    }
}



// MARK: Selected category List View
extension BreakFastVC {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("DID SELECT row: \(indexPath.row), section: \(indexPath.section)")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
class DynamicCollectionView: UICollectionView {

    override func layoutSubviews() {
        super.layoutSubviews()
        if bounds.size != intrinsicContentSize {
            invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        return self.contentSize
    }
}

extension BreakFastVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.size.width
        let height = width/10
        return CGSize(width: width, height: height)
    }
}

class ValueCollectionViewCell: UICollectionViewCell {
  @IBOutlet weak var catName: UILabel!
  @IBOutlet weak var price: UILabel!
  @IBOutlet weak var rank: UILabel!
  @IBOutlet weak var other: UILabel!
    
    var entity : Category_listModel!{
        didSet {
            catName.text = entity.name
            price.text = "\(entity.price)"
            rank.text = entity.rank
            other.text = entity.other
            
        }
    }
}

extension BreakFastVC: UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return catListdataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ValueCollectionViewCell", for: indexPath) as! ValueCollectionViewCell
            cell.entity = catListdataSource[indexPath.row]
        if catListdataSource[indexPath.row].rank?.toInt() == 1{
            cell.catName.textColor = .red
            cell.price.textColor = .red
            cell.rank.textColor = .red
            cell.other.textColor = .red
        }
        else if catListdataSource[indexPath.row].rank?.toInt() == catListdataSource.count{
            cell.catName.textColor = .green
            cell.price.textColor = .green
            cell.rank.textColor = .green
            cell.other.textColor = .green
        }
        else{
            cell.catName.textColor = .white
            cell.price.textColor = .white
            cell.rank.textColor = .white
            cell.other.textColor = .white}
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}




