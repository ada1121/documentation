//
//  SplashVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/24/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        gotoMainpage()

        // Do any additional setup after loading the view.
    }
    
    func gotoMainpage()  {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            nextVC.modalPresentationStyle = .fullScreen
                 self.present(nextVC, animated: true, completion: nil)
                    
            }
        )
    }
}
